using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGeneration : MonoBehaviour
{
    [SerializeField] GameObject theGround;
    [SerializeField] Transform generationPoint;
    [SerializeField] float distanceBetween;

    private float platformWidth;

    void Start()
    {
        platformWidth = theGround.GetComponent<BoxCollider2D>().size.x;
    }

    void Update()
    {
        if(transform.position.x < generationPoint.position.x) 
        {
            transform.position = new Vector3(transform.position.x + platformWidth + distanceBetween, transform.position.y, transform.position.z);

            Instantiate (theGround, transform.position, transform.rotation);
        }
    }
}
