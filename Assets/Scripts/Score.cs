using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    public int currentScore;

    DinoMovement dino;

    [SerializeField] TextMeshProUGUI currentScoreTMPro;
    [SerializeField] TextMeshProUGUI hiScoreTMPro;

    void Start()
    {
        dino = GameObject.FindObjectOfType<DinoMovement>();

        currentScore = 0;
        ScorePrint();
        HiScorePrint();
        StartCoroutine(ScoreIncrementer());
    }

    void ScorePrint()
    {
        currentScoreTMPro.text = ScoreFormat(currentScore);
    }


    IEnumerator ScoreIncrementer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            if(dino != null && dino.isDead == false && dino.hasStarted == true) 
            {
                currentScore++;
                ScorePrint();
            }
        }
    }


    private string ScoreFormat(int myScore)
    {
       switch (myScore) {
           case int n when n < 10:
               return "0000" + myScore.ToString();
            case int n when n < 100:
               return "000" + myScore.ToString();
            case int n when n < 1000:
               return "00" + myScore.ToString();
            case int n when n < 10000:
               return "0" + myScore.ToString();
           default :
               return myScore.ToString();
       }
    }

     public void UpdateHiScore(int newScore)
    {
        PlayerPrefs.SetInt("highscore", newScore);
        HiScorePrint();
    }

    void HiScorePrint()
    {
        int hiScore = 0;
        if(PlayerPrefs.HasKey("highscore")) 
        {
            hiScore = PlayerPrefs.GetInt("highscore");
        }
        hiScoreTMPro.text = ScoreFormat(hiScore); 
    }

    private void OnDestroy() 
    {
        PlayerPrefs.Save();
    }
}