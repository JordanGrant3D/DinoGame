using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CactusGeneration : MonoBehaviour
{
    [SerializeField] GameObject[] cactusArray;
    [SerializeField] GameObject cactusCollider;
    [SerializeField] Transform generationPoint;
    [SerializeField] float randomLocationAddition;

    private float cactusWidth;
    private float distanceBetween;

    int cactusIndex;

    private void Start()
    {
        ChooseRandomCactus();

        cactusWidth = cactusCollider.GetComponent<BoxCollider2D>().size.x;
    }

    void Update()
    {
        if(transform.position.x  < generationPoint.position.x)
        {
            SetRandomLocation();
            ChooseRandomCactus();
            transform.position = new Vector3(transform.position.x + cactusWidth + distanceBetween, transform.position.y, transform.position.z);

            Instantiate (cactusArray[cactusIndex], transform.position, transform.rotation);
        }
    }

    void ChooseRandomCactus()
    {
        cactusIndex = Random.Range(0, cactusArray.Length);
    }

    void SetRandomLocation()
    {
        randomLocationAddition = Random.Range(11f, 15f);
        distanceBetween = randomLocationAddition;
    }
}
