using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DinoMovement : MonoBehaviour
{
    private float currentMoveSpeed;
    public float moveSpeed;
    [SerializeField] float jumpSpeed = 10f;

    public Rigidbody2D rb2D;
    public BoxCollider2D boxCollider2D;
    public Animator animator2D;

    public bool hasStarted;
    public bool isDead = false;

    Score score;

    void Start()
    {
        score = GameObject.FindObjectOfType<Score>();
        hasStarted = false;
        currentMoveSpeed = moveSpeed;
        moveSpeed = 0f;
        animator2D.SetBool("notStarting", true);
    }


    void FixedUpdate()
    {
        if(hasStarted == false && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow))) 
        {
            hasStarted = true;
            moveSpeed = currentMoveSpeed;
        }
        else if((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow))) 
        {
            Jump();
        }

        Movement();
        IncreaseSpeed();

        if(hasStarted == true) 
        {
            animator2D.SetBool("notStarting", false);
        }
    }

    void Movement()
    {
        if(isDead == true) 
        {
            return;
        }

        transform.Translate(moveSpeed, 0f, 0f * Time.deltaTime);
    }

    private bool isOnGround()
    {
        return boxCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")) ? true : false;
    }

    void Jump()
    {
        if(isDead == true || hasStarted == false) 
        {
            return;
        }

        if(isOnGround())
        {
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, jumpSpeed, 0);
        }
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Cactus") 
        {
            IsDead();
        }
    }

    void IsDead()
    {
        isDead = true;
        animator2D.SetBool("isDead", true);

        Debug.Log("You are dead");

        if(PlayerPrefs.GetInt("highscore") < score.currentScore)
        {
            score.UpdateHiScore(score.currentScore);
        } 
        StartCoroutine(LoadDeathScene());
    }

    IEnumerator LoadDeathScene()
    {
        yield return new WaitForSecondsRealtime(1);

        SceneManager.LoadScene(1);
    }

    
   void IncreaseSpeed() 
    {
        float increaseSpeed = 0.0005f;
        
        if(score.currentScore > 0) 
        {
            moveSpeed = score.currentScore % 10 == 0 ? (moveSpeed + increaseSpeed) : moveSpeed;
        }
    }
    

}