using System.Collections;  
using System.Collections.Generic;  
using UnityEngine;  
using UnityEngine.SceneManagement;  

public class SceneControl : MonoBehaviour 
{  
    // Play's the game
    public void Play() 
    {  
        SceneManager.LoadScene(0);  
    } 

    // Loads the play again scene
    public void Death() 
    {  
        SceneManager.LoadScene(1);  
    } 

    // Quits the application
    public void Quit() 
    {  
        Application.Quit();
    }  
} 