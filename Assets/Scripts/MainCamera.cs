using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    [SerializeField] Transform Target;
    [SerializeField] Transform Camera;
    Vector3 tempVec3 = new Vector3();

    void LateUpdate()
    {
        if (Target != null)
        {
            tempVec3.x = (Target.position.x) + 4.0f;
            tempVec3.z = 0;
            tempVec3.z = this.transform.position.z;
            this.transform.position = tempVec3;
        }
        else if (Target == null)
        {
            tempVec3.x = Camera.position.x;
            tempVec3.z = Camera.transform.position.y;
            tempVec3.z = Camera.transform.position.z;
            Camera.transform.position = tempVec3;
        }
    }
}
