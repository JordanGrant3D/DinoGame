## Name
Dinosaur Game 

## Description
This is a clone of the Google no internet Dinosaur Game all the assets were made in "Krita" the game was put together in "Unity" and the IDE was "Visual Studio Code" it is an "Endless Runner"


## Installation
Unity Download: https: //unity3d.com/get-unity/download
Visual Studio Code Download: https: //code.visualstudio.com/download
Krita Download: https://krita.org/en/download/krita-desktop/

## Support
Channel
https://www.youtube.com/channel/UCqUuZ6WXP0hvRGo8F_rbzRA

Email
jordangrant3d@gmail.com